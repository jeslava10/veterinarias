CREATE TABLE  "roles_usuarios"(

 id_roles integer NOT NULL,
 descripcion character varying(100)   NOT NULL,
PRIMARY KEY (id_roles)

);

ALTER TABLE "roles_usuarios" OWNER to postgres;

CREATE TABLE  "tipo_documentos"(

 id_tipo_documento integer NOT NULL,
 descripcion character varying(100)   NOT NULL,
 PRIMARY KEY (id_tipo_documento)

);

ALTER TABLE "tipo_documentos" OWNER to postgres;

CREATE TABLE  "usuarios"
(
    id_usuario serial NOT NULL,
    nombres_usuario character varying(100)   NOT NULL,
    apellidos_usuario character varying(100)   NOT NULL,
    fk_tipo_documento integer   NOT NULL,
    numero_documento_usuario character varying(100)   NOT NULL,
    departamento_residencia_usuario character varying(100)  ,
    ciudad_residencia_usuario character varying(100)  ,
    barrio_residencia_usuario character varying(100)  ,
    telefono_usuario character varying(100)  ,
    correo_usuario character varying(200)   NOT NULL,
    contrasena_usuario character varying(50)   NOT NULL,
    estado_usuario boolean NOT NULL,
    fk_rol_usuario integer NOT NULL,
    fecha_nacimiento_usuario date,
    PRIMARY KEY (id_usuario)

);

alter table "usuarios"
      add constraint fk_rol_user
      foreign key (fk_rol_usuario)
      references "roles_usuarios" (id_roles);

alter table "usuarios"
      add constraint fk_tipo_doc_user
      foreign key (fk_tipo_documento)
      references "tipo_documentos" (id_tipo_documento);

ALTER TABLE "usuarios" OWNER to postgres;

CREATE UNIQUE INDEX uInd_valor_documento
ON "usuarios"(numero_documento_usuario);

CREATE TABLE "mascotas"
(
    id_mascota serial NOT NULL,
    nombre_mascota character varying(100)   NOT NULL,
    especie_mascota character varying(100)   NOT NULL,
    edad_mascota integer NOT NULL,
    raza_mascota character varying(100)   NOT NULL,
    sexo_mascota character varying(10)   NOT NULL,
    observaciones character varying(2000)  ,
    estado_mascota boolean NOT NULL,
    fk_duenio integer ,
    PRIMARY KEY (id_mascota)

);

alter table "mascotas"
      add constraint fk_duenio_user
      foreign key (fk_duenio)
      references "usuarios" (id_usuario);

ALTER TABLE "mascotas" OWNER to postgres;

CREATE TABLE "veterinarias"(
    id_veterinaria  serial NOT NULL,
    nombre_veterinaria character varying(200)   NOT NULL,
    nit_veterinaria character varying(100)   NOT NULL,
    correo_veterinaria character varying(100)   NOT NULL,
    estado_veterinaria boolean NOT NULL,
    fk_duenio_veterinaria integer,
    PRIMARY KEY (id_veterinaria)
);

alter table "veterinarias"
      add constraint fk_duenio_veterinaria
      foreign key (fk_duenio_veterinaria)
      references "usuarios" (id_usuario);

ALTER TABLE "veterinarias"  OWNER to postgres;

CREATE TABLE "sucursales"
(
    id_sucursal serial NOT NULL,
    direccion_sucursal character varying(100)   NOT NULL,
    telefono_sucursal character varying(100)   NOT NULL,
    imagen_sucursal bytea,
    fk_admin_sucursal integer,
    fk_veterinaria integer,
    PRIMARY KEY (id_sucursal)
);

alter table "sucursales"
      add constraint fk_admin_sucursal
      foreign key (fk_admin_sucursal)
      references "usuarios" (id_usuario);

alter table "sucursales"
      add constraint fk_veterinaria
      foreign key (fk_veterinaria)
      references "veterinarias" (id_veterinaria);


ALTER TABLE "sucursales"   OWNER to postgres;
