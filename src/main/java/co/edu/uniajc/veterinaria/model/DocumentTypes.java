package co.edu.uniajc.veterinaria.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_documentos")
public class DocumentTypes {

    @Id
    @Column(name = "id_tipo_documento")
    private Long id;

    @Column(name = "descripcion")
    private String description;

    public DocumentTypes() {
    }

    public DocumentTypes(Long id) {
        this.id = id;
    }

    public DocumentTypes(Long id, String description) {
        this.id = id;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

