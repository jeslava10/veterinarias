package co.edu.uniajc.veterinaria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "usuarios")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long id;

    @Column(name = "nombres_usuario")
    private String name;

    @Column(name = "apellidos_usuario")
    private String lastName;

    @OneToOne
    @JoinColumn(name = "fk_tipo_documento", referencedColumnName = "id_tipo_documento")
    private DocumentTypes documentTypes;

    @Column(name = "numero_documento_usuario")
    private String documentNumber;

    @Column(name = "departamento_residencia_usuario")
    private String departmentResidence;

    @Column(name = "ciudad_residencia_usuario")
    private String cityResidence;

    @Column(name = "barrio_residencia_usuario")
    private String neighborhoodResidence;

    @Column(name = "telefono_usuario")
    private String phone;

    @Column(name = "correo_usuario")
    private String email;

    @Column(name = "contrasena_usuario")
    private String password;

    @Column(name = "estado_usuario")
    private Boolean state;

    @OneToOne
    @JoinColumn(name = "fk_rol_usuario", referencedColumnName = "id_roles")
    private UsersRoles usersRoles;

    @Column(name = "fecha_nacimiento_usuario")
    private Date birthdate;

    public Users() {
    }

    public Users(Long id, String name, String lastName, DocumentTypes documentTypes, String documentNumber, String departmentResidence, String cityResidence, String neighborhoodResidence, String phone, String email, String password, Boolean state, UsersRoles usersRoles, Date birthdate) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.documentTypes = documentTypes;
        this.documentNumber = documentNumber;
        this.departmentResidence = departmentResidence;
        this.cityResidence = cityResidence;
        this.neighborhoodResidence = neighborhoodResidence;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.state = state;
        this.usersRoles = usersRoles;
        this.birthdate = birthdate;
    }

    public Users(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public DocumentTypes getDocumentTypes() {
        return documentTypes;
    }

    public void setDocumentTypes(DocumentTypes documentTypes) {
        this.documentTypes = documentTypes;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDepartmentResidence() {
        return departmentResidence;
    }

    public void setDepartmentResidence(String departmentResidence) {
        this.departmentResidence = departmentResidence;
    }

    public String getCityResidence() {
        return cityResidence;
    }

    public void setCityResidence(String cityResidence) {
        this.cityResidence = cityResidence;
    }

    public String getNeighborhoodResidence() {
        return neighborhoodResidence;
    }

    public void setNeighborhoodResidence(String neighborhoodResidence) {
        this.neighborhoodResidence = neighborhoodResidence;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public UsersRoles getUsersRoles() {
        return usersRoles;
    }

    public void setUsersRoles(UsersRoles usersRoles) {
        this.usersRoles = usersRoles;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}