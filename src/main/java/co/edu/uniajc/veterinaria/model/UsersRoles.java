package co.edu.uniajc.veterinaria.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "roles_usuarios")
public class UsersRoles {

    @Id
    @Column(name = "id_roles")
    private Long id;

    @Column(name = "descripcion")
    private String description;

    public UsersRoles() {
    }

    public UsersRoles(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public UsersRoles(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
