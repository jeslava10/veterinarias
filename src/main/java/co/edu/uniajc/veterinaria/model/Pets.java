package co.edu.uniajc.veterinaria.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mascotas")
public class Pets {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mascota")
    private Long id;

    @Column(name = "nombre_mascota")
    private String namePet;

    @Column(name = "especie_mascota")
    private String speciesPet;

    @Column(name = "edad_mascota")
    private Integer agePet;

    @Column(name = "raza_mascota")
    private String breedPet;

    @Column(name = "sexo_mascota")
    private String sexPet;

    @Column(name = "observaciones")
    private String observationsPet;

    @Column(name = "estado_mascota")
    private Boolean statusPet;

    @OneToOne
    @JoinColumn(name = "fk_duenio", referencedColumnName = "id_usuario")
    private Users users;

    public Pets() {

    }

    public Pets(Long id, String namePet, String speciesPet, Integer agePet, String breedPet, String sexPet, String observationsPet, Boolean statusPet, Users users) {
        this.id = id;
        this.namePet = namePet;
        this.speciesPet = speciesPet;
        this.agePet = agePet;
        this.breedPet = breedPet;
        this.sexPet = sexPet;
        this.observationsPet = observationsPet;
        this.statusPet = statusPet;
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamePet() {
        return namePet;
    }

    public void setNamePet(String namePet) {
        this.namePet = namePet;
    }

    public String getSpeciesPet() {
        return speciesPet;
    }

    public void setSpeciesPet(String speciesPet) {
        this.speciesPet = speciesPet;
    }

    public Integer getAgePet() {
        return agePet;
    }

    public void setAgePet(Integer agePet) {
        this.agePet = agePet;
    }

    public String getBreedPet() {
        return breedPet;
    }

    public void setBreedPet(String breedPet) {
        this.breedPet = breedPet;
    }

    public String getSexPet() {
        return sexPet;
    }

    public void setSexPet(String sexPet) {
        this.sexPet = sexPet;
    }

    public String getObservationsPet() {
        return observationsPet;
    }

    public void setObservationsPet(String observationsPet) {
        this.observationsPet = observationsPet;
    }

    public Boolean getStatusPet() {
        return statusPet;
    }

    public void setStatusPet(Boolean statusPet) {
        this.statusPet = statusPet;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
}