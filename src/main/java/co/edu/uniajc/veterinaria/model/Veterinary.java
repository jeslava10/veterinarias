package co.edu.uniajc.veterinaria.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "veterinarias")
public class Veterinary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_veterinaria")
    private Long id;

    @Column(name = "nombre_veterinaria")
    private String name;

    @Column(name = "nit_veterinaria")
    private String nit;

    @Column(name = "correo_veterinaria")
    private String mail;

    @Column(name = "estado_veterinaria")
    private Boolean state;

    @OneToOne
    @JoinColumn(name = "fk_duenio_veterinaria", referencedColumnName = "id_usuario")
    private Users users;

    public Veterinary() {
        //Construct
    }

    public Veterinary(Long id, String name, String nit, String mail, Boolean state, Users users) {
        this.id = id;
        this.name = name;
        this.nit = nit;
        this.mail = mail;
        this.state = state;
        this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
}
