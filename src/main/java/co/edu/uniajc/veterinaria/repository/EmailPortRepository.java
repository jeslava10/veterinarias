package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.model.EmailBody;

public interface EmailPortRepository {

    public boolean sendEmail(EmailBody emailBody);

}
