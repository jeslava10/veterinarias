package co.edu.uniajc.veterinaria.repository;


import co.edu.uniajc.veterinaria.model.Pets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PetsRepository extends JpaRepository<Pets, Long> {

    List<Pets> findAllByNamePet(String namePet);

}
