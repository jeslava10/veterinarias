package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.model.DocumentTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentTypesRepository extends JpaRepository<DocumentTypes, Long> {

}
