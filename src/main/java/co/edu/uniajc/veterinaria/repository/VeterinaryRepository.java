package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.model.Veterinary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VeterinaryRepository extends JpaRepository<Veterinary, Long> {


}
