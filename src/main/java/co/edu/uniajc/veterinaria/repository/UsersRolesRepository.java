package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.model.UsersRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UsersRolesRepository extends JpaRepository<UsersRoles, Long> {


}
