package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.model.Veterinary;
import co.edu.uniajc.veterinaria.service.VeterinaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/veterinary")
public class VeterinaryController {

    private final VeterinaryService veterinaryService;

    @Autowired
    public VeterinaryController(VeterinaryService veterinaryService) {
        this.veterinaryService = veterinaryService;
    }

    @PostMapping(path = "/save")
    public Veterinary saveVeterinary(@RequestBody Veterinary veterinary) {
        return veterinaryService.saveVeterinary(veterinary);
    }

    @PutMapping(path = "/update")
    public Veterinary updateVeterinary(@RequestBody Veterinary veterinary) {
        return veterinaryService.updateVeterinary(veterinary);
    }

    @GetMapping(path = "/all")
    public List<Veterinary> findAllVeterinary() {
        return veterinaryService.findAllVeterinary();
    }

}
