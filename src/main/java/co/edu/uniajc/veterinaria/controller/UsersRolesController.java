package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.model.UsersRoles;
import co.edu.uniajc.veterinaria.service.UsersRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/usersRoles")
public class UsersRolesController {

    private final UsersRolesService usersRolesService;

    @Autowired
    public UsersRolesController(UsersRolesService usersRolesService) {
        this.usersRolesService = usersRolesService;
    }

    @PostMapping(path = "/save")
    public UsersRoles saveUsersRoles(@RequestBody UsersRoles usersRoles) {
        return usersRolesService.saveUsersRoles(usersRoles);
    }

    @PutMapping(path = "/update")
    public UsersRoles uptadeUsersRoles(@RequestBody UsersRoles useresRoles) {
        return usersRolesService.updateUsersRoles(useresRoles);
    }

    @GetMapping(path = "/all")
    public List<UsersRoles> findAllUsersRoles() {
        return usersRolesService.findAllUsersRoles();
    }

}
