package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.model.Users;
import co.edu.uniajc.veterinaria.service.JwtUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UsersController {

    private final JwtUsersService jwtUsersService;

    @Autowired
    public UsersController(JwtUsersService jwtUsersService) {

        this.jwtUsersService = jwtUsersService;
    }

    @PostMapping(path = "/save")
    public Users saveUsers(@RequestBody Users users) {
        return jwtUsersService.saveUsers(users);
    }

    @PutMapping(path = "/update")
    public Users uptadeUsers(@RequestBody Users users) {
        return jwtUsersService.updateUsers(users);
    }

    @GetMapping(path = "/all")
    public List<Users> findAllUsers() {
        return jwtUsersService.findAllUsers();
    }

    @PostMapping(path = "/emailChangePasswoord")
    public String changePasswoord(@RequestParam("email") String email) {
        Optional<Users> user = jwtUsersService.findByEmailUsers(email);
        if (user.isPresent()) {
            jwtUsersService.sendChangePasswordMail(user);
        } else {
            return "error el usuario no existe";
        }
        return "enviado";
    }

    @PostMapping(path = "/changePasswoord")
    public Users newPasswoord(@RequestParam("email") String email, @RequestParam("password") String password) {
        Optional<Users> user = jwtUsersService.findByEmailUsers(email);
        Users users = user.get();
        users.setPassword(password);
        users.setEmail(email);
        return jwtUsersService.updateUsers(users);

    }


}
