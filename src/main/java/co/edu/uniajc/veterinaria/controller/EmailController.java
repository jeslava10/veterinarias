package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.model.EmailBody;
import co.edu.uniajc.veterinaria.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/email")
public class EmailController {

    @Autowired
    private EmailService emailService;

    @PostMapping(value = "/send")
    @ResponseBody
    public boolean SendEmail(@RequestBody EmailBody emailBody) {
        return emailService.sendEmail(emailBody);
    }

}
