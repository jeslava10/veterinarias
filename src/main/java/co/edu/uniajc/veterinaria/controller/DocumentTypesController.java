package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.model.DocumentTypes;
import co.edu.uniajc.veterinaria.service.DocumentTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/documentTypes")
public class DocumentTypesController {

    private final DocumentTypesService documentTypesService;

    @Autowired
    public DocumentTypesController(DocumentTypesService documentTypesService) {
        this.documentTypesService = documentTypesService;
    }

    @PostMapping(path = "/save")
    public DocumentTypes saveDocumentTypes(@RequestBody DocumentTypes documentTypes) {
        return documentTypesService.saveDocumentTypes(documentTypes);
    }

    @PutMapping(path = "/update")
    public DocumentTypes updateDocumentTypes(@RequestBody DocumentTypes documentTypes) {
        return documentTypesService.updateDocumentTypes(documentTypes);
    }

    @GetMapping(path = "/all")
    public List<DocumentTypes> findAllDocumentType() {
        return documentTypesService.findAllDocumentTypes();
    }


}
