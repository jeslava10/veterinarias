package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.model.Pets;
import co.edu.uniajc.veterinaria.service.PetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/pets")
public class PetsController {

    private final PetsService petsService;

    @Autowired
    public PetsController(PetsService petsService) {

        this.petsService = petsService;
    }

    @PostMapping(path = "/save")
    public Pets savePets(@RequestBody Pets pets) {

        return petsService.savePets(pets);
    }

    @PutMapping(path = "/update")
    public Pets uptadePets(@RequestBody Pets pets) {

        return petsService.updatePets(pets);
    }

    @GetMapping(path = "/all/name")
    public List<Pets> findAllByNamePet(@RequestParam(name = "name") String namePet) {
        return petsService.findAllByNamePet(namePet);
    }
}
