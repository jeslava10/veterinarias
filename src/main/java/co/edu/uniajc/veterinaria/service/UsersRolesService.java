package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.model.UsersRoles;
import co.edu.uniajc.veterinaria.repository.UsersRolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersRolesService {

    private UsersRolesRepository usersRolesRepository;

    @Autowired
    public UsersRolesService(UsersRolesRepository usersRolesRepository) {
        this.usersRolesRepository = usersRolesRepository;
    }

    public UsersRoles saveUsersRoles(UsersRoles usersRoles) {
        return usersRolesRepository.save(usersRoles);
    }

    public UsersRoles updateUsersRoles(UsersRoles usersRoles) {
        return usersRolesRepository.save(usersRoles);
    }

    public List<UsersRoles> findAllUsersRoles() {
        return usersRolesRepository.findAll();
    }


}
