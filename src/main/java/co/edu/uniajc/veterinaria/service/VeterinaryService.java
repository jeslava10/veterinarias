package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.model.Veterinary;
import co.edu.uniajc.veterinaria.repository.VeterinaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeterinaryService {

    private VeterinaryRepository veterinaryRepository;

    @Autowired
    public VeterinaryService(VeterinaryRepository veterinaryRepository) {
        this.veterinaryRepository = veterinaryRepository;
    }

    public Veterinary saveVeterinary(Veterinary veterinary) {
        return veterinaryRepository.save(veterinary);
    }

    public Veterinary updateVeterinary(Veterinary veterinary) {
        return veterinaryRepository.save(veterinary);
    }

    public List<Veterinary> findAllVeterinary() {
        return veterinaryRepository.findAll();
    }
}

