package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.model.EmailBody;
import co.edu.uniajc.veterinaria.model.Users;
import co.edu.uniajc.veterinaria.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtUsersService implements UserDetailsService {

    private UsersRepository usersRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    public JwtUsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Users> user = usersRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + email);
        }
        return new org.springframework.security.core.userdetails.User(user.get().getEmail(), user.get().getPassword(),
                new ArrayList<>());
    }

    public Users saveUsers(Users users) {
        users.setPassword(bcryptEncoder.encode(users.getPassword()));
        return usersRepository.save(users);
    }

    public Users updateUsers(Users users) {
        users.setPassword(bcryptEncoder.encode(users.getPassword()));
        return usersRepository.save(users);
    }

    public List<Users> findAllUsers() {

        return usersRepository.findAll();
    }

    public Optional<Users> findByEmailUsers(String email) {
        return usersRepository.findByEmail(email);
    }

    public void sendChangePasswordMail(Optional<Users> user) {

        EmailBody emailBody = new EmailBody();

        emailBody.setEmail(user.get().getEmail());
        emailBody.setSubject("Cambio de contraseña");
        emailBody.setContent("Hola " + user.get().getName() + user.get().getLastName() +
                "Recientemente solicitó restablecer su contraseña para su cuenta en <Nombre_Veterinaria>."
                + "Si tiene alguna duda por favor escribir al correo solo pulsando el icono de correo"
                + "http://localhost:8889/api/veterinaria/users/changePasswoord");
        emailService.sendEmail(emailBody);
    }
}