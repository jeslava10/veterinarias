package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.model.DocumentTypes;
import co.edu.uniajc.veterinaria.repository.DocumentTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentTypesService {

    private DocumentTypesRepository documentTypesRepository;

    @Autowired
    public DocumentTypesService(DocumentTypesRepository documentTypesRepository) {
        this.documentTypesRepository = documentTypesRepository;
    }

    public DocumentTypes saveDocumentTypes(DocumentTypes documentTypes) {
        return documentTypesRepository.save(documentTypes);
    }

    public DocumentTypes updateDocumentTypes(DocumentTypes documentTypes) {
        return documentTypesRepository.save(documentTypes);
    }

    public List<DocumentTypes> findAllDocumentTypes() {
        return documentTypesRepository.findAll();
    }


}
