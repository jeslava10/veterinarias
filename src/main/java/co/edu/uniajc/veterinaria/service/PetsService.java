package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.model.Pets;
import co.edu.uniajc.veterinaria.repository.PetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PetsService {

    private final PetsRepository petsRepository;

    @Autowired
    public PetsService(PetsRepository petsRepository) {
        this.petsRepository = petsRepository;
    }

    public Pets savePets(Pets pets) {
        return petsRepository.save(pets);
    }

    public Pets updatePets(Pets pets) {
        return petsRepository.save(pets);
    }

    public List<Pets> findAllByNamePet(String namePet) {
        return petsRepository.findAllByNamePet(namePet);
    }
}
