package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.factories.VeterinaryFactory;
import co.edu.uniajc.veterinaria.model.Veterinary;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class VeterinaryRepositoryUnitTests {

    @Autowired
    private VeterinaryRepository veterinaryRepository;

    @Test
    public void testSaveVeterinary() {
        Veterinary veterinary = new VeterinaryFactory().newInstance();
        veterinaryRepository.save(veterinary);
        Optional<Veterinary> veterinary2 = veterinaryRepository.findById(veterinary.getId());
        assertNotNull(veterinary);
        assertEquals(veterinary2.get().getId(), veterinary.getId());
        assertEquals(veterinary2.get().getName(), veterinary.getName());
    }

    @Test
    public void findAllVeterinary() {
        Veterinary veterinary = new VeterinaryFactory().setName("Hullitas Amor").newInstance();
        veterinaryRepository.save(veterinary);
        assertNotNull(veterinaryRepository.findAll());
    }

    @Test
    public void testUpdateVeterinary() {
        Veterinary veterinary = new VeterinaryFactory().newInstance();
        veterinaryRepository.save(veterinary);
        Optional<Veterinary> veterinary2 = veterinaryRepository.findById(veterinary.getId());
        veterinary2.get().setName("Huellas salud");
        Veterinary veterinary3 = veterinary2.get();
        assertNotNull(veterinary3);
        assertEquals(veterinary3.getId(), veterinary2.get().getId());
        assertEquals(veterinary3.getName(), veterinary2.get().getName());
    }

}