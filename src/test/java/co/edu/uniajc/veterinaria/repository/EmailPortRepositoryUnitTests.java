package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.factories.EmailBodyFactory;
import co.edu.uniajc.veterinaria.model.EmailBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailPortRepositoryUnitTests {

    @Autowired
    private EmailPortRepository emailPortRepository;

    @Test
    public void testsEndEmail() {
        EmailBody emailBody = new EmailBodyFactory().newInstance();
        Boolean enviado = emailPortRepository.sendEmail(emailBody);
        assertEquals(enviado, true);
    }


}