package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.factories.UsersRolesFactory;
import co.edu.uniajc.veterinaria.model.UsersRoles;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UsersRolesRepositoryUnitTests {

    @Autowired
    private UsersRolesRepository usersRolesRepository;

    @Test
    public void testSaveUsersRoles() {

        UsersRoles usersRoles = new UsersRolesFactory().setDescription("Mantenimiento").newInstance();
        usersRolesRepository.save(usersRoles);
        Optional<UsersRoles> usersRoles2 = usersRolesRepository.findById(usersRoles.getId());
        assertNotNull(usersRoles);
        assertEquals(usersRoles2.get().getId(), usersRoles.getId());
        assertEquals(usersRoles2.get().getDescription(), usersRoles.getDescription());
    }

    @Test
    public void findAllUsersRoles() {
        UsersRoles usersRoles = new UsersRolesFactory().setDescription("Mantenimiento").newInstance();
        usersRolesRepository.save(usersRoles);
        assertNotNull(usersRolesRepository.findAll());
    }

    @Test
    public void testUpdateUsersRoles() {

        UsersRoles usersRoles = new UsersRolesFactory().setDescription("Mantenimiento").newInstance();
        usersRolesRepository.save(usersRoles);
        Optional<UsersRoles> usersRoles2 = usersRolesRepository.findById(usersRoles.getId());
        UsersRoles usersRoles3 = usersRolesRepository.save(new UsersRoles(usersRoles2.get().getId(), "Axuliar Mantemiento"));
        assertNotNull(usersRoles3);
        assertEquals(usersRoles3.getId(), usersRoles.getId());
        assertEquals(usersRoles3.getDescription(), "Axuliar Mantemiento");

    }

}