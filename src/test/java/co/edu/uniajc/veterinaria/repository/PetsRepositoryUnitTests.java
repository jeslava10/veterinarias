package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.factories.PetsFactory;
import co.edu.uniajc.veterinaria.model.Pets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PetsRepositoryUnitTests {

    @Autowired
    private PetsRepository petsRepository;

    @Test
    public void testSavePetsRepository() {

        Pets pets = new PetsFactory().newInstance();
        petsRepository.save(pets);
        Optional<Pets> pets2 = petsRepository.findById(pets.getId());
        assertNotNull(pets);
        assertEquals(pets2.get().getId(), pets.getId());
        assertEquals(pets2.get().getNamePet(), pets.getNamePet());
    }

    @Test
    public void testUpdatePetsRepository() {

        Pets pets = new PetsFactory().newInstance();
        petsRepository.save(pets);
        Optional<Pets> pets2 = petsRepository.findById(pets.getId());
        pets2.get().setNamePet("Hullitas");
        Pets pets3 = petsRepository.save(pets2.get());
        assertNotNull(pets3);
        assertEquals(pets3.getId(), pets2.get().getId());
        assertEquals(pets3.getNamePet(), pets2.get().getNamePet());

    }

}