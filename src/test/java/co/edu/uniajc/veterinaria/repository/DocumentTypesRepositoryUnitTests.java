package co.edu.uniajc.veterinaria.repository;

import co.edu.uniajc.veterinaria.factories.DocumentTypesFactory;
import co.edu.uniajc.veterinaria.model.DocumentTypes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DocumentTypesRepositoryUnitTests {

    @Autowired
    private DocumentTypesRepository documentTypesRepository;

    @Test
    public void testSaveDocumentTypes() {

        DocumentTypes documentTypes = new DocumentTypesFactory().newInstance();
        documentTypesRepository.save(documentTypes);
        Optional<DocumentTypes> documentTypes2 = documentTypesRepository.findById(documentTypes.getId());
        assertNotNull(documentTypes);
        assertEquals(documentTypes2.get().getId(), documentTypes.getId());
        assertEquals(documentTypes2.get().getDescription(), documentTypes.getDescription());
    }

    @Test
    public void findAllDocumentTypes() {
        DocumentTypes documentTypes = new DocumentTypesFactory().setDescription("europa passport").newInstance();
        documentTypesRepository.save(documentTypes);
        assertNotNull(documentTypesRepository.findAll());
    }

    @Test
    public void testUpdateDocumentTypes() {

        DocumentTypes documentTypes = new DocumentTypesFactory().setDescription("europa passport").newInstance();
        documentTypesRepository.save(documentTypes);
        Optional<DocumentTypes> documentTypes2 = documentTypesRepository.findById(documentTypes.getId());
        DocumentTypes documentTypes3 = documentTypesRepository.save(new DocumentTypes(documentTypes2.get().getId(), "spain passport"));
        assertNotNull(documentTypes3);
        assertEquals(documentTypes3.getId(), documentTypes.getId());
        assertEquals(documentTypes3.getDescription(), "spain passport");

    }

}