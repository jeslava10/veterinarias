package co.edu.uniajc.veterinaria.factories;

import co.edu.uniajc.veterinaria.model.EmailBody;

public class EmailBodyFactory {

    private String email;
    private String content;
    private String subject;


    public EmailBodyFactory() {
        // Initialize the factory with default values
        email = "juaneslava56@gmail.com";
        content = "Prueba";
        subject = "Prueba";

    }

    public EmailBody newInstance() {
        return new EmailBody(email, content, subject);
    }

    public EmailBodyFactory setEmail(String email) {
        this.email = email;
        return this;
    }

    public EmailBodyFactory setContent(String content) {
        this.content = content;
        return this;
    }

    public EmailBodyFactory setSubject(String subject) {
        this.subject = subject;
        return this;
    }
}
