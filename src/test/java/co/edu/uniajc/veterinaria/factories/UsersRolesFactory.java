package co.edu.uniajc.veterinaria.factories;

import co.edu.uniajc.veterinaria.model.UsersRoles;

public class UsersRolesFactory {

    private Long id;
    private String description;

    public UsersRolesFactory() {
        // Initialize the factory with default values
        id = (long) 3;
        description = "Vet";
    }

    public UsersRoles newInstance() {
        return new UsersRoles(id, description);
    }

    public UsersRolesFactory setDescription(String description) {
        this.description = description;
        return this;
    }

    public UsersRolesFactory setId(long id) {
        this.id = id;
        return this;
    }


}
