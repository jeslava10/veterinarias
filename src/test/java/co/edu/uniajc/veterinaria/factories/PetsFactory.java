package co.edu.uniajc.veterinaria.factories;

import co.edu.uniajc.veterinaria.model.Pets;
import co.edu.uniajc.veterinaria.model.Users;

public class PetsFactory {

    private Long id;
    private String namePet;
    private String speciesPet;
    private Integer agePet;
    private String breedPet;
    private String sexPet;
    private String observationsPet;
    private Boolean statusPet;
    private Users users;


    public PetsFactory() {
        // Initialize the factory with default values
        id = (long) 40;
        namePet = "Lucas";
        speciesPet = "Perro";
        agePet = 4;
        breedPet = "pincher";
        sexPet = "M";
        observationsPet = "Tiene manchas  negras y su color en blanco";
        statusPet = true;
        users = new Users((long) 1);

    }

    public Pets newInstance() {
        return new Pets(id,
                namePet,
                speciesPet,
                agePet,
                breedPet,
                sexPet,
                observationsPet,
                statusPet,
                users);
    }

    public PetsFactory setId(Long id) {
        this.id = id;
        return this;
    }

    public PetsFactory setNamePet(String namePet) {
        this.namePet = namePet;
        return this;
    }

    public PetsFactory setSpeciesPet(String speciesPet) {
        this.speciesPet = speciesPet;
        return this;
    }

    public PetsFactory setAgePet(Integer agePet) {
        this.agePet = agePet;
        return this;
    }

    public PetsFactory setBreedPet(String breedPet) {
        this.breedPet = breedPet;
        return this;
    }

    public PetsFactory setSexPet(String sexPet) {
        this.sexPet = sexPet;
        return this;
    }

    public PetsFactory setObservationsPet(String observationsPet) {
        this.observationsPet = observationsPet;
        return this;
    }

    public PetsFactory setStatusPet(Boolean statusPet) {
        this.statusPet = statusPet;
        return this;
    }

    public PetsFactory setUsers(Users users) {
        this.users = users;
        return this;
    }
}
