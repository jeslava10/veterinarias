package co.edu.uniajc.veterinaria.factories;

import co.edu.uniajc.veterinaria.model.Users;
import co.edu.uniajc.veterinaria.model.Veterinary;

public class VeterinaryFactory {

    private Long id;
    private String name;
    private String nit;
    private String mail;
    private Boolean state;
    private Users users;

    public VeterinaryFactory() {
        // Initialize the factory with default values
        id = (long) 11;
        name = "huellitas";
        nit = "9875714741";
        mail = "veterinaria@gmail.com";
        state = true;
        users = new Users((long) 1);
    }

    public Veterinary newInstance() {
        return new Veterinary(id, name, nit, mail, state, users);
    }

    public VeterinaryFactory setId(Long id) {
        this.id = id;
        return this;
    }

    public VeterinaryFactory setName(String name) {
        this.name = name;
        return this;
    }

    public VeterinaryFactory setNit(String nit) {
        this.nit = nit;
        return this;
    }

    public VeterinaryFactory setMail(String mail) {
        this.mail = mail;
        return this;
    }

    public VeterinaryFactory setState(Boolean state) {
        this.state = state;
        return this;
    }

    public VeterinaryFactory setUsers(Users users) {
        this.users = users;
        return this;
    }
}
