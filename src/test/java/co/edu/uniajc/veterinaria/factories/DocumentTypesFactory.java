package co.edu.uniajc.veterinaria.factories;

import co.edu.uniajc.veterinaria.model.DocumentTypes;

public class DocumentTypesFactory {

    private Long id;
    private String description;

    public DocumentTypesFactory() {
        // Initialize the factory with default values
        id = (long) 5;
        description = "Usa Passport";
    }

    public DocumentTypes newInstance() {
        return new DocumentTypes(id, description);
    }

    public DocumentTypesFactory setDescription(String description) {
        this.description = description;
        return this;
    }

    public DocumentTypesFactory setId(long id) {
        this.id = id;
        return this;
    }


}
