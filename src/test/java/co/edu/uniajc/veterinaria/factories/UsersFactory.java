package co.edu.uniajc.veterinaria.factories;

import co.edu.uniajc.veterinaria.model.DocumentTypes;
import co.edu.uniajc.veterinaria.model.Pets;
import co.edu.uniajc.veterinaria.model.Users;
import co.edu.uniajc.veterinaria.model.UsersRoles;

import java.util.Date;

public class UsersFactory {

    private Long id;
    private String name;
    private String lastName;
    private DocumentTypes documentTypes;
    private String documentNumber;
    private String departmentResidence;
    private String cityResidence;
    private String neighborhoodResidence;
    private String phone;
    private String email;
    private String password;
    private Boolean state;
    private UsersRoles usersRoles;
    private Date birthdate;

    public UsersFactory() {
        // Initialize the factory with default values
        id = (long) 5;
        name = "John";
        lastName = "Idrobo";
        documentTypes = new DocumentTypes((long)1);
        documentNumber = "100254789";
        departmentResidence = "Valle";
        cityResidence = "Palmira";
        neighborhoodResidence = "carrera 33";
        phone = "3002538702";
        email = "js04@gmail.com";
        password = "js04";
        state = true;
        usersRoles = new UsersRoles((long)1);
        birthdate = null;
    }

    public Users newInstance() {
        return new Users(id,
                name,
                lastName,
                documentTypes,
                documentNumber,
                departmentResidence,
                cityResidence,
                neighborhoodResidence,
                phone,
                email,
                password,
                state,
                usersRoles,
                birthdate
                );
    }

    public UsersFactory setId(Long id) {
        this.id = id;
        return this;
    }

    public UsersFactory setName(String name) {
        this.name = name;
        return this;
    }

    public UsersFactory setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UsersFactory setDocumentTypes(DocumentTypes documentTypes) {
        this.documentTypes = documentTypes;
        return this;
    }

    public UsersFactory setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
        return this;
    }

    public UsersFactory setDepartmentResidence(String departmentResidence) {
        this.departmentResidence = departmentResidence;
        return this;
    }

    public UsersFactory setCityResidence(String cityResidence) {
        this.cityResidence = cityResidence;
        return this;
    }

    public UsersFactory setNeighborhoodResidence(String neighborhoodResidence) {
        this.neighborhoodResidence = neighborhoodResidence;
        return this;
    }

    public UsersFactory setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public UsersFactory setEmail(String email) {
        this.email = email;
        return this;
    }

    public UsersFactory setPassword(String password) {
        this.password = password;
        return this;
    }

    public UsersFactory setState(Boolean state) {
        this.state = state;
        return this;
    }

    public UsersFactory setUserRoles(UsersRoles userRoles) {
        this.usersRoles = usersRoles;
        return this;
    }

    public UsersFactory setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
        return this;
    }
}
