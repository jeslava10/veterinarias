package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.factories.PetsFactory;
import co.edu.uniajc.veterinaria.model.Pets;
import co.edu.uniajc.veterinaria.service.PetsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PetsControllerUnitTests {

    @Autowired
    private PetsService petsService;

    private Pets pets = new PetsFactory().newInstance();

    @Test
    public void savePets() {
        Pets pets2 = petsService.savePets(pets);
        assertNotNull(pets2);
        assertEquals(pets2.getId(), pets.getId());
        assertEquals(pets2.getNamePet(), pets.getNamePet());
    }

    @Test
    public void findAllByNamePet() {
        List<Pets> petsObject = petsService.findAllByNamePet(pets.getNamePet());
        assertNotNull(pets);
        assertEquals(petsObject.get(0).getNamePet(), pets.getNamePet());
    }

    @Test
    public void UpdatePets() {
        Pets pets2 = petsService.updatePets(new PetsFactory().setNamePet("kato").newInstance());
        assertNotNull(pets);
        assertEquals(pets2.getId(), pets.getId());
    }

}
