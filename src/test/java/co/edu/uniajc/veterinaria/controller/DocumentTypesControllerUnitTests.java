package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.factories.DocumentTypesFactory;
import co.edu.uniajc.veterinaria.model.DocumentTypes;
import co.edu.uniajc.veterinaria.service.DocumentTypesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DocumentTypesControllerUnitTests {

    @Autowired
    private DocumentTypesService documentTypesService;

    private DocumentTypes documentTypes = new DocumentTypesFactory().newInstance();


    @Test
    public void saveDocumentTypes() {

        DocumentTypes documentTypesObject = documentTypesService.saveDocumentTypes(documentTypes);
        assertNotNull(documentTypes);
        assertEquals(documentTypesObject.getId(), documentTypes.getId());
        assertEquals(documentTypesObject.getDescription(), documentTypes.getDescription());
    }

    @Test
    public void findAllDocumentTypes() {
        List<DocumentTypes> documentTypesObject = documentTypesService.findAllDocumentTypes();
        assertNotNull(documentTypes);
        assertEquals(documentTypesObject.get(2).getDescription(), documentTypes.getDescription());
    }

    @Test
    public void UpdateDocumentTypes() {
        DocumentTypes documentTypesObject2 = documentTypesService.updateDocumentTypes(new DocumentTypesFactory().setDescription("spain passport").newInstance());
        assertNotNull(documentTypes);
        assertEquals(documentTypesObject2.getId(), documentTypes.getId());
        assertEquals(documentTypesObject2.getDescription(), "spain passport");
    }

}