package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.factories.VeterinaryFactory;
import co.edu.uniajc.veterinaria.model.*;
import co.edu.uniajc.veterinaria.service.VeterinaryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
public class VeterinaryControllerUnitTests {

    @Autowired
    private VeterinaryService veterinaryService;

    private Veterinary veterinary = new VeterinaryFactory().newInstance();

    @Test
    public void saveVeterinary() {
        Veterinary veterinary2 = veterinaryService.saveVeterinary(veterinary);
        assertNotNull(veterinary2);
        assertEquals(veterinary2.getId(), veterinary.getId());
        assertEquals(veterinary2.getNit(), veterinary.getNit());
    }


    @Test
    public void findAllVeterinary() {
        List<Veterinary> veterinaryObject = veterinaryService.findAllVeterinary();
        assertNotNull(veterinary);
        assertEquals(veterinaryObject.get(10).getNit(), veterinary.getNit());
    }


    @Test
    public void updateVeterinary() {
        Veterinary veterinary2 = veterinaryService.updateVeterinary(new VeterinaryFactory().setMail("huellas@huella.com").newInstance());
        assertNotNull(veterinary2);
        assertEquals(veterinary2.getId(), veterinary.getId());
        assertEquals(veterinary2.getNit(), veterinary.getNit());
    }
}