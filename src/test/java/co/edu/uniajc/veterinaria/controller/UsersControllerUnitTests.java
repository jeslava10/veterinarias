package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.factories.UsersFactory;
import co.edu.uniajc.veterinaria.model.Users;
import co.edu.uniajc.veterinaria.service.JwtUsersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersControllerUnitTests {

    @Autowired
    private JwtUsersService usersService;

    private Users users = new UsersFactory().newInstance();

    @Test
    public void saveUsers() {
        Users users2 = usersService.saveUsers(users);
        assertNotNull(users2);
        assertEquals(users2.getDocumentNumber(), users.getDocumentNumber());
    }


    @Test
    public void findAllUsers() {
        List<Users> usersObject = usersService.findAllUsers();
        assertNotNull(users);
        assertEquals(usersObject.get(3).getDocumentNumber(), "100254789");
    }


  @Test
    public void uptadeUsers() {
        Users users2 = usersService.updateUsers(new UsersFactory().setName("Juan").newInstance());
        assertNotNull(users);
        assertEquals(users2.getId(), users.getId());
    }
}