package co.edu.uniajc.veterinaria.controller;

import co.edu.uniajc.veterinaria.factories.UsersRolesFactory;
import co.edu.uniajc.veterinaria.model.UsersRoles;
import co.edu.uniajc.veterinaria.service.UsersRolesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersRolesControllerUnitTests {

    @Autowired
    private UsersRolesService usersRolesService;

    private UsersRoles usersRoles = new UsersRolesFactory().newInstance();


    @Test
    public void saveUsersRoles() {
        UsersRoles usersRolesObject = usersRolesService.saveUsersRoles(usersRoles);
        assertNotNull(usersRoles);
        assertEquals(usersRolesObject.getId(), usersRoles.getId());
        assertEquals(usersRolesObject.getDescription(), usersRoles.getDescription());
    }

    @Test
    public void findAllUsersRoles() {
        List<UsersRoles> usersRolesObject = usersRolesService.findAllUsersRoles();
        assertNotNull(usersRoles);
        assertEquals(usersRolesObject.get(2).getId(), usersRoles.getId());
        assertEquals(usersRolesObject.get(2).getDescription(), "Vet");
    }


    @Test
    public void uptadeUsersRoles() {
        UsersRoles usersRolesObject2 = usersRolesService.updateUsersRoles(new UsersRolesFactory().setDescription("Adm").newInstance());
        assertNotNull(usersRoles);
        assertEquals(usersRolesObject2.getId(), usersRoles.getId());
        assertEquals(usersRolesObject2.getDescription(), "Adm");
    }

}