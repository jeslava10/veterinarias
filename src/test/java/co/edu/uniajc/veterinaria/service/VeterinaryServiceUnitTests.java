package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.factories.VeterinaryFactory;
import co.edu.uniajc.veterinaria.model.Veterinary;
import co.edu.uniajc.veterinaria.repository.VeterinaryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class VeterinaryServiceUnitTests {

    @Autowired
    private VeterinaryService veterinaryService;

    @MockBean
    private VeterinaryRepository veterinaryRepository;

    @Test
    public void testCreateVeterinary() {
        Veterinary veterinary = new VeterinaryFactory().newInstance();
        Mockito.when(veterinaryRepository.save(veterinary)).thenReturn(veterinary);
        assertThat(veterinaryService.saveVeterinary(veterinary)).isEqualTo(veterinary);

    }

    @Test
    public void testGetAllVeterinary() {
        Veterinary veterinary = new VeterinaryFactory().newInstance();
        Veterinary veterinary2 = new VeterinaryFactory().setId((long) 1).setName("Huella de amor").newInstance();
        List<Veterinary> veterinaryList = new ArrayList<>();
        veterinaryList.add(veterinary);
        veterinaryList.add(veterinary2);
        Mockito.when(veterinaryRepository.findAll()).thenReturn(veterinaryList);
        assertThat(veterinaryService.findAllVeterinary()).isEqualTo(veterinaryList);
    }


    @Test
    public void testUpdateVeterinary() {
        Veterinary veterinary = new VeterinaryFactory().newInstance();
        Mockito.when(veterinaryRepository.findById((long) 11)).thenReturn(Optional.of(veterinary));
        veterinary.setName("Huella de amor S.A.S");
        Mockito.when(veterinaryRepository.save(veterinary)).thenReturn(veterinary);
        assertThat(veterinaryService.updateVeterinary(veterinary).equals(veterinary));

    }


}
