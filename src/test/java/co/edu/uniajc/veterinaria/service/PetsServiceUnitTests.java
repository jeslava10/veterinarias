package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.factories.PetsFactory;
import co.edu.uniajc.veterinaria.model.Pets;
import co.edu.uniajc.veterinaria.repository.PetsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class PetsServiceUnitTests {

    @Autowired
    private PetsService petsService;

    @MockBean
    private PetsRepository petsRepository;

    @Test
    public void testCreatePets() {

        Pets pets = new PetsFactory().newInstance();
        Mockito.when(petsRepository.save(pets)).thenReturn(pets);
        assertThat(petsService.savePets(pets)).isEqualTo(pets);
    }

    @Test
    public void testUpdatePets() {
        Pets pets = new PetsFactory().newInstance();
        Mockito.when(petsRepository.findById((long) 40)).thenReturn(Optional.of(pets));
        pets.setNamePet("Pedro marquez");
        Mockito.when(petsRepository.save(pets)).thenReturn(pets);
        assertThat(petsService.updatePets(pets).equals(pets));
    }


}
