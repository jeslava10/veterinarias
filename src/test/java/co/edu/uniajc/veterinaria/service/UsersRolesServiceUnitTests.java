package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.factories.UsersRolesFactory;
import co.edu.uniajc.veterinaria.model.UsersRoles;
import co.edu.uniajc.veterinaria.repository.UsersRolesRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersRolesServiceUnitTests {

    @Autowired
    private UsersRolesService usersRolesService;

    @MockBean
    private UsersRolesRepository usersRolesRepository;

    @Test
    public void testCreateUsersRoles() {
        UsersRoles usersRoles = new UsersRolesFactory().newInstance();
        Mockito.when(usersRolesRepository.save(usersRoles)).thenReturn(usersRoles);
        assertThat(usersRolesService.saveUsersRoles(usersRoles)).isEqualTo(usersRoles);
    }

    @Test
    public void testGetAllUsersRoles() {
        UsersRoles usersRoles = new UsersRolesFactory().newInstance();
        UsersRoles usersRoles2 = new UsersRolesFactory().setId((long) 1).setDescription("Auxiliar").newInstance();
        List<UsersRoles> usersRolesList = new ArrayList<>();
        usersRolesList.add(usersRoles);
        usersRolesList.add(usersRoles2);
        Mockito.when(usersRolesRepository.findAll()).thenReturn(usersRolesList);
        assertThat(usersRolesService.findAllUsersRoles()).isEqualTo(usersRolesList);
    }

    @Test
    public void testUpdateUsersRoles() {
        UsersRoles usersRoles = new UsersRolesFactory().newInstance();
        Mockito.when(usersRolesRepository.findById((long) 3)).thenReturn(Optional.of(usersRoles));
        usersRoles.setDescription("aux Mantenimiento");
        Mockito.when(usersRolesRepository.save(usersRoles)).thenReturn(usersRoles);
        assertThat(usersRolesService.updateUsersRoles(usersRoles).equals(usersRoles));
    }

}
