package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.factories.EmailBodyFactory;
import co.edu.uniajc.veterinaria.model.EmailBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceUnitTests {

    @MockBean
    private EmailService emailService;

    @Test
    public void testSendEmail() {
        EmailBody emailBody = new EmailBodyFactory().newInstance();
        Boolean sent = emailService.sendEmail(emailBody);
        assertEquals(sent, false);

    }

}
