package co.edu.uniajc.veterinaria.service;

import co.edu.uniajc.veterinaria.factories.DocumentTypesFactory;
import co.edu.uniajc.veterinaria.model.DocumentTypes;
import co.edu.uniajc.veterinaria.repository.DocumentTypesRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DocumentTypesServiceUnitTests {

    @Autowired
    private DocumentTypesService documentTypesService;

    @MockBean
    private DocumentTypesRepository documentTypesRepository;

    @Test
    public void testCreateDocumentTypes() {

        DocumentTypes documentTypes = new DocumentTypesFactory().newInstance();
        Mockito.when(documentTypesRepository.save(documentTypes)).thenReturn(documentTypes);
        assertThat(documentTypesService.saveDocumentTypes(documentTypes)).isEqualTo(documentTypes);

    }

    @Test
    public void testGetAllDocumentTypes() {

        DocumentTypes documentTypes = new DocumentTypesFactory().newInstance();
        DocumentTypes documentTypes2 = new DocumentTypesFactory().setId(1).setDescription("Cedula").newInstance();
        List<DocumentTypes> documentTypesList = new ArrayList<>();
        documentTypesList.add(documentTypes);
        documentTypesList.add(documentTypes2);

        Mockito.when(documentTypesRepository.findAll()).thenReturn(documentTypesList);
        assertThat(documentTypesService.findAllDocumentTypes()).isEqualTo(documentTypesList);
    }


    @Test
    public void testUpdateDocumentTypes() {
        DocumentTypes documentTypes = new DocumentTypesFactory().newInstance();
        Mockito.when(documentTypesRepository.findById((long) 5)).thenReturn(Optional.of(documentTypes));
        documentTypes.setDescription("usa passport");
        Mockito.when(documentTypesRepository.save(documentTypes)).thenReturn(documentTypes);
        assertThat(documentTypesService.updateDocumentTypes(documentTypes).equals(documentTypes));

    }


}
